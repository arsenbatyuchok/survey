This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

I tried to keep it as simple as possible, so I did not use Redux or any other state management tool.

I used [mockable.io](http://mockable.io) to provide mocked data and imitate real API.

## Installation & Setup
`yarn install`

`yarn start`

## Running unit tests
`yarn test` to run tests and `yarn test -- --coverage` to see coverage

## Time spent
~ 20-25 hrs

## Things I would like to fix/cover but didn't due to lack of time:
1. Auto-suggest for states input
2. Error handling for forms - they only show one error - 'This field is required'
3. Error handling for API requests + tests for it
4. Better flexibility for Survey styles, i.e. survey panel has hardcoded height
5. Re-iterate through Survey component and refactor all the 'noise' :)
6. fix all other TODOs in code
7. create a better mobile UX
8. format Phone Number component value when setting it to WizardForm state (it is set as string (123) 11... but not number)

P.S. There's Avenir Next font used on mockups, it's not available free of charge, so if you don't have it on your system you will see fallback font.

Thanks!