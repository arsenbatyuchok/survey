import { fetchData } from './api';

class MockClient {
    static url = 'http://demo6603181.mockable.io';

    static constructFormsUrl() {
        return `${MockClient.url}/forms`;
    }

    static getForms() {
        return fetchData(MockClient.constructFormsUrl());
    }
}

export default MockClient;