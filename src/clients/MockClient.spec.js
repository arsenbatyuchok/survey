import fetchMock from 'fetch-mock';
import MockClient from './MockClient';
import * as api from './api';

describe('MockClient', () => {
    const testResponse = { some: 'data' };
    const mockSuccessResponse = {
        status: 200,
        body: testResponse
    };

    afterEach(() => {
        jest.clearAllMocks();
        fetchMock.restore();
    });

    describe('Forms', () => {
        const formsUrl = MockClient.constructFormsUrl();
        const spy = jest.spyOn(api, 'fetchData');
        
        it('gets Forms data', () => {
            fetchMock.get(formsUrl, mockSuccessResponse);
            
            return MockClient.getForms().then((forms) => {
                expect(spy).toBeCalledWith(formsUrl);
                expect(forms).toEqual(testResponse);
            });
        });
    });
    
});