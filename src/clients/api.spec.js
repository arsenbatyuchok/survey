import fetchMock from 'fetch-mock';
import * as api from './api';

describe('API', () => {
    const URL = '*';
    const mockSuccess = {
        body: {},
        status: 200,
        headers: { 'content-type': 'application/json' }
    };

    const mockFailure = {
        status: 503,
        headers: { 'content-type': 'application/json' }
    };

    afterEach(() => {
        fetchMock.restore();
    });

    it('fetches data with success', () => {
        fetchMock.get(URL, mockSuccess);
        expect(api.fetchData(URL)).resolves.toBeDefined();
    });

    it('fetches data with fail', () => {
        fetchMock.get(URL, mockFailure);
        expect(api.fetchData(URL)).rejects.toBeDefined();
    });
});
