import React from 'react';

const Footer = () => (
    <footer className="footer">
        <div className="footer-license">
            Matic Insurance Services License number in California is 0I92444
        </div>
        <div className="footer-links">
            <a href="/">All licenses</a>
            <span className="footer-separator">|</span>
            <a href="/">Privacy policy</a>
        </div>
    </footer>
);


export default Footer;