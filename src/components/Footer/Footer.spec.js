import React from 'react';
import { shallow } from 'enzyme';
import Footer from './Footer';

describe('Footer', () => {
    it('renders itself and children', () => {
        const renderedComponent = shallow(<Footer />);

        expect(renderedComponent.length).toEqual(1);
        expect(renderedComponent.find('.footer-license').length).toEqual(1);
        expect(renderedComponent.find('.footer-links').length).toEqual(1);
        expect(renderedComponent.find('a').length).toEqual(2);
    });
});