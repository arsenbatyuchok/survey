import React from 'react';
import { shallow, mount } from 'enzyme';
import WizardForm from './WizardForm';

describe('WizardForm', () => {
    const props = {
        title: 'someWizardText',
        fields: [{
            name: 'name',
            label: 'labelName',
            type: 'text',
            validation: "^.{3,128}$"
        }],
        onSubmit: () => {},
    }
    const mockOnSubmit = jest.fn();
    
    it('renders itself', () => {
        const renderedComponent = shallow(<WizardForm {...props} />);
        const title = renderedComponent.find('.wizard-section-title');

        expect(renderedComponent.length).toEqual(1);
        expect(renderedComponent.find('form').length).toEqual(1);
        expect(renderedComponent.find('button').length).toEqual(1);
        expect(title.text()).toEqual('someWizardText');
    });

    it('sets state based on props fields', () => {
        const renderedComponent = shallow(<WizardForm {...props} />);

        expect(renderedComponent.state().values).toHaveProperty(props.fields[0].name);
        expect(renderedComponent.state().errors).toHaveProperty(props.fields[0].name);
        expect(renderedComponent.state().errors.name).toEqual(false);
    });

    it('handles validation', () => {
        const newProps = {
            ...props, 
            onSubmit: mockOnSubmit
        };
        const renderedComponent = mount(<WizardForm {...newProps} />);

        renderedComponent.find('[type="submit"]').simulate('submit');
        renderedComponent.update();
        expect(renderedComponent.state().errors.name).toEqual(true);
        expect(mockOnSubmit).not.toHaveBeenCalled();
    });
    
    it('handles successful onSubmit', () => {
        const newProps = {
            ...props, 
            onSubmit: mockOnSubmit
        };
        const renderedComponent = mount(<WizardForm {...newProps} />);


        renderedComponent.find('input[name="name"]').simulate('change', {
            target: {
                value: 'Test name'
            }
        });
        renderedComponent.find('[type="submit"]').simulate('submit');
        expect(renderedComponent.find('button').length).toEqual(1);
        expect(mockOnSubmit).toBeCalledWith(renderedComponent.state().values);
    });
});