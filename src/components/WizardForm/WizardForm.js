import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PhoneInput from '../PhoneInput/PhoneInput';
import TextInput from '../TextInput/TextInput';
import FormGroup from '../FormGroup/FormGroup';

class WizardForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            values: WizardForm.createInitialStateFromProps('', props.fields),
            errors: WizardForm.createInitialStateFromProps(false, props.fields)
        }

        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    static createInitialStateFromProps(initialValue, fields) {
        // adds initial values for fields, like: 
        // this.state = { values: { firstField: initialValue, second: initialValue } }
        return fields.reduce((acc, item) => {
            acc[item.name] = initialValue;
            return acc;
        }, {});
    }

    static inputTypeMap(props) {
        return {
            text: <TextInput {...props} />,
            phone: <PhoneInput {...props} />,
            state: <TextInput {...props} /> // TODO replace with autocomplete
        };
    }

    // TODO move to some utils
    static testValueValidity(value, validation) {
        return new RegExp(validation).test(value);
    }

    static isFormValid(errorState) {
        return Object.values(errorState).indexOf(true) > -1;
    }

    onFormSubmit(event) {
        const { values } = this.state;
        const { fields } = this.props;
        const newErrorState = this.getErrorStatus(fields, values);

        event.preventDefault();
        this.setState({ errors: newErrorState });

        if (WizardForm.isFormValid(newErrorState)) return;
        this.props.onSubmit(values);
    }

    getErrorStatus(fields, values) {
        return fields.reduce((acc, item) => {
            if (!item.validation) return acc;
            acc[item.name] = !WizardForm.testValueValidity(values[item.name], item.validation);
            return acc;
        }, {});
    }

    setFieldError(name, value) {
        const errors = {...this.state.errors};

        errors[name] = value;
        this.setState({ errors });
    }

    onInputChange(name, e) {
        const values = {...this.state.values};

        values[name] = e.target.value;
        // removing error when input gets changed again
        this.setFieldError(name, false);
        this.setState({ values });
    }

    render() {
        const { errors } = this.state;
        const { fields } = this.props;

        return (
            <form className="wizard-form" onSubmit={this.onFormSubmit}>
                <div className="wizard-section-title">
                    {this.props.title}
                </div>
                <div className="form-row">
                    {fields.map(({ name, label, type }, i) =>
                        <FormGroup hasError={errors[name]} name={name} label={label} key={i}>
                            {WizardForm.inputTypeMap({
                                name,
                                id: name,
                                onChange: this.onInputChange.bind(this, name)
                            })[type]}
                        </FormGroup>
                    )}
                </div>
                <button type="submit" className="button button-primary wizard-section-button">
                    {this.props.buttonText}
                </button>
            </form>
        );
    }
};

WizardForm.propTypes = {
    fields: PropTypes.array.isRequired,
    onSubmit: PropTypes.func.isRequired,
    title: PropTypes.string,
    buttonText: PropTypes.string
};

WizardForm.defaultProps = {
    fields: [],
    title: '',
    buttonText: 'Next',
};

export default WizardForm;