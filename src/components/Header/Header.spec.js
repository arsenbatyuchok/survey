import React from 'react';
import { shallow } from 'enzyme';
import Header from './Header';

describe('Header', () => {
    it('renders itself and children', () => {
        const renderedComponent = shallow(<Header />);

        expect(renderedComponent.length).toEqual(1);
        expect(renderedComponent.find('.logo').length).toEqual(1);
    });
});