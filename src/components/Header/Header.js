import React from 'react';
import { logoMedium, logoLarge } from '../../constants/constants';

const Header = () => (
    <header className="header">
        <div className="header-logo">
            <a href="/">
                <img srcSet={`${logoMedium} 1x, ${logoLarge} 2x`} 
                        src={logoMedium}
                        className="logo" 
                        alt="Matic" />
            </a>
        </div>
        <div className="header-banner"></div>
    </header>
);

export default Header;