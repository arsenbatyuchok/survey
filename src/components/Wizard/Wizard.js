import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

class Wizard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentIdx: 0
        };

        this.onNext = this.onNext.bind(this);
    }

    componentDidMount() {
        this.updateProgress(this.state.currentIdx);
    }

    onNext() {
        const currentIdx = this.state.currentIdx + 1;
        
        this.setState({ currentIdx });
        this.updateProgress(currentIdx);
    }

    updateProgress(index) {
        const { sections } = this.props;
        const completion = (index + 1) / sections.length * 100;

        this.props.onSectionChange(completion);
    }

    renderImage() {
        const { src, srcSet } = this.props.image;
        return (
            <div className="wizard-image">
                <img srcSet={srcSet} src={src} alt="Name of the guy" />
            </div>
        );
    }

    renderSections() {
        const { sections } = this.props;

        return sections.map((section, key) =>
            this.state.currentIdx === sections.indexOf(section) &&
                <CSSTransition timeout={400} key={key} classNames="wizard">
                    <div className="wizard-section">
                        {this.props.children(key, section, this.onNext)}
                    </div>
                </CSSTransition>
        );
    }

    render() {
        return (

            <div className="wizard-wrapper">
                {this.props.image && this.renderImage()}
                <TransitionGroup className="wizard">
                    {this.renderSections()}
                </TransitionGroup>
            </div>
            
        );
    }
}

Wizard.propTypes = {
    sections: PropTypes.array,
    onComplete: PropTypes.func,
    image: PropTypes.object,
    onSectionChange: PropTypes.func,
};

Wizard.defaultProps = {
    sections: [],
    image: undefined,
    onComplete: () => {},
    onSectionChange: () => {}
};

export default Wizard;