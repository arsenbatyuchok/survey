import React from 'react';
import { shallow, mount } from 'enzyme';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import Wizard from './Wizard';

const childSection = (idx, section, onNext) => (
    <div className="child-section" id={`section-${idx}`}>
        <div className="section-title">{section.title}</div>
        <button type="button" onClick={onNext}>click</button>
    </div>
);

describe('Wizard', () => {
    const props = {
        sections: [
            { title: 'first title' },
            { title: 'second title' }
        ],
        onComplete: () => {},
        onSectionChange: () => {}
    };
    let renderedComponent; 
    

    it('renders itself with default state', () => {
        renderedComponent = shallow(
            <Wizard {...props}>
                {childSection}
            </Wizard>
        );

        expect(renderedComponent.length).toEqual(1);
        expect(renderedComponent.find(TransitionGroup).length).toEqual(1);
        expect(renderedComponent.state().currentIdx).toEqual(0);
    });

    it('renders child sections', () => {
        renderedComponent = mount(
            <Wizard {...props}>
                {childSection}
            </Wizard>
        );
        const child = renderedComponent.find('.child-section');

        expect(renderedComponent.find(CSSTransition).length).toEqual(1);
        expect(child.length).toEqual(1);
        expect(child.find('.section-title').text()).toEqual('first title');
    });

    it('goes to next section when onNext triggered', () => {
        renderedComponent = mount(
            <Wizard {...props}>
                {childSection}
            </Wizard>
        );
        const child = renderedComponent.find('.child-section');

        renderedComponent.find('button').simulate('click');
        renderedComponent.update();
        expect(renderedComponent.state().currentIdx).toEqual(1);
    });
});