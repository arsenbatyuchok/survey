import React from 'react';
import { shallow } from 'enzyme';
import TextInput from './TextInput';

describe('TextInput', () => {
    const props = {
        name: 'firstName',
        id: 'uniqueId'
    }
    it('renders itself', () => {
        const renderedComponent = shallow(<TextInput {...props} />);

        expect(renderedComponent.length).toEqual(1);
        expect(renderedComponent.prop('name')).toEqual('firstName');
        expect(renderedComponent.prop('id')).toEqual('uniqueId');
    });

    it('handles onChange', () => {
        const mockOnChange = jest.fn();
        const event = { target: '' };
        const newProps = {...props, onChange: mockOnChange};
        const renderedComponent = shallow(<TextInput {...newProps} />);

        renderedComponent.simulate('change', event);
        expect(mockOnChange).toBeCalledWith(event);
    });
});