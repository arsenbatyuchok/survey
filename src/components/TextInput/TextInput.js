import React from 'react';
import PropTypes from 'prop-types';

const TextInput = props =>
    <input type="text" className="form-control" {...props} />;

TextInput.propTypes = {
    name: PropTypes.string,
    id: PropTypes.string,
    onChange: PropTypes.func,
};

TextInput.defaultProps = {
    name: '',
    id: '',
    onChange: () => {}
};

export default TextInput;