import React from 'react';
import { shallow } from 'enzyme';
import WizardText from './WizardText';

describe('WizardText', () => {
    const props = {
        text: () => 'someWizardText',
        onNext: () => {},
    }
    const mockOnNext = jest.fn();
    
    it('renders itself', () => {
        const renderedComponent = shallow(<WizardText {...props} />);
        const title = renderedComponent.find('.wizard-section-title');

        expect(renderedComponent.length).toEqual(1);
        expect(title.text()).toEqual('someWizardText');
    });

    it('handles onNext', () => {
        const newProps = {
            ...props, 
            onNext: mockOnNext
        };
        const renderedComponent = shallow(<WizardText {...newProps} />);
        
        renderedComponent.find('button').simulate('click');
        expect(renderedComponent.find('button').length).toEqual(1);
        expect(mockOnNext).toBeCalled();
    });
});