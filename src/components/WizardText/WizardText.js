import React from 'react';
import PropTypes from 'prop-types';

const WizardText = (props) => {
    return (
        <div className="wizard-text">
            <div className="wizard-section-title has-margin">
                {props.text(props.textData)}
            </div>
            {props.onNext && 
                <button type="button" 
                    onClick={props.onNext} 
                    className="button button-primary wizard-section-button">
                        {props.buttonText}
                </button>
            }
        </div>
    );
};

WizardText.propTypes = {
    text: PropTypes.func.isRequired,
    buttonText: PropTypes.string,
    onNext: PropTypes.func,
    textData: PropTypes.object
};

WizardText.defaultProps = {
    buttonText: 'Next',
    onNext: null,
    textData: {}
};

export default WizardText;