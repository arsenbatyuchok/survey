import React from 'react';
import { shallow } from 'enzyme';
import Progress from './Progress';

describe('Progress', () => {
    it('renders itself with default status', () => {
        const renderedComponent = shallow(<Progress />);
        const status = renderedComponent.find('.progress-status');

        expect(renderedComponent.length).toEqual(1);
        expect(status.props().style.width).toEqual('0%');
    });

    it('renders status based on props value', () => {
        const renderedComponent = shallow(<Progress status={50} />);
        const status = renderedComponent.find('.progress-status');

        expect(status.props().style.width).toEqual('50%');
    });
});