import React from 'react';
import PropTypes from 'prop-types';

const getProgressStyles = status => ({ width: `${status}%` });

const Progress = props => (
    <div className="progress">
        <div className="progress-status" style={getProgressStyles(props.status)}></div>
    </div>
);

Progress.propTypes = {
    status: PropTypes.number
};

Progress.defaultProps = {
    status: 0
};

export default Progress;