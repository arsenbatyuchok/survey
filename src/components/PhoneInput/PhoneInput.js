import React from 'react';
import PropTypes from 'prop-types';
import InputMask from 'react-input-mask';

const PhoneInput = props =>
    <InputMask {...props} 
        className="form-control" 
        type="tel" 
        mask="(999) 999-9999"
        placeholder="(___) ___-____" />;

PhoneInput.propTypes = {
    name: PropTypes.string,
    id: PropTypes.string,
    onChange: PropTypes.func
};
PhoneInput.defaultProps = {
    name: '',
    id: '',
    onChange: () => {}
};

export default PhoneInput;