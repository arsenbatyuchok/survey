import React from 'react';
import { shallow, mount } from 'enzyme';
import InputMask from 'react-input-mask';
import PhoneInput from './PhoneInput';

describe('PhoneInput', () => {
    const props = {
        name: 'phoneNumber',
        id: 'uniqueId'
    }

    it('renders itself', () => {
        const renderedComponent = shallow(<PhoneInput {...props} />);
        
        expect(renderedComponent.length).toEqual(1);
        expect(renderedComponent.find(InputMask).length).toEqual(1);
        expect(renderedComponent.prop('name')).toEqual('phoneNumber');
        expect(renderedComponent.prop('id')).toEqual('uniqueId');
    });

    it('handles onChange', () => {
        const mockOnChange = jest.fn();
        const event = { target: '' };
        const newProps = {...props, onChange: mockOnChange}
        const renderedComponent = mount(<PhoneInput {...newProps} />);
        
        renderedComponent.simulate('change', event);
        expect(mockOnChange).toBeCalled();
    });
});