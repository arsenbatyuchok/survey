import React from 'react';
import PropTypes from 'prop-types';
import { ERROR_MESSAGE_DEFAULT } from '../../constants/constants';

const getFormGroupClass = (hasError) => {
    let classList = ['form-group'];
    if (hasError) {
        classList.push('has-error');
    }
    return classList.join(' ');
};

const FormGroup = props => (
    <div className={getFormGroupClass(props.hasError)}>
        <label htmlFor={props.name}>
            {props.label}
        </label>
        {props.children}
        { props.hasError && <div className="error-message">{props.errorMessage}</div> }
    </div>
);

FormGroup.propTypes = {
    name: PropTypes.string.isRequired,
    hasError: PropTypes.bool,
    errorMessage: PropTypes.string
};
FormGroup.defaultProps = {
    name: '',
    hasError: false,
    errorMessage: ERROR_MESSAGE_DEFAULT
};

export default FormGroup;