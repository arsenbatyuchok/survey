import React from 'react';
import { shallow, mount } from 'enzyme';
import FormGroup from './FormGroup';
import { ERROR_MESSAGE_DEFAULT } from '../../constants/constants';

describe('FormGroup', () => {
    const props = {
        name: 'formGroupName',
        hasError: false,
        errorMessage: undefined
    }
    const Child = () => <div className="some-child-class">Child component</div>;

    it('renders itself and children', () => {
        const renderedComponent = mount(
            <FormGroup {...props}>
                <Child />
            </FormGroup>
        );

        expect(renderedComponent.length).toEqual(1);
        expect(renderedComponent.find(`[htmlFor="${props.name}"]`).length).toEqual(1);
        expect(renderedComponent.find('.some-child-class').length).toEqual(1);
    });

    it('renders with error and default error message', () => {
        const newProps = { ...props, hasError: true };
        const renderedComponent = shallow(<FormGroup {...newProps} />);
        const errorMessage = renderedComponent.find('.error-message');

        expect(renderedComponent.hasClass('has-error')).toEqual(true);
        expect(errorMessage.length).toEqual(1);
        expect(errorMessage.text()).toEqual(ERROR_MESSAGE_DEFAULT);
    });

    it('renders with custom error message', () => {
        const newProps = { ...props, hasError: true, errorMessage: 'error' };
        const renderedComponent = shallow(<FormGroup {...newProps} />);

        expect(renderedComponent.find('.error-message').text()).toEqual('error');
    });
});