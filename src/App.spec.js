import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

describe('App', () => {
    it('renders App and it\'s children', () => {
        const renderedComponent = shallow(<App />);
    
        expect(renderedComponent.find('.outer-wrapper').length).toEqual(1);
    });

    it('sets progress status', () => {
        const renderedComponent = shallow(<App />);

        renderedComponent.instance().setProgressStatus(10);
        expect(renderedComponent.state().progressStatus).toEqual(10);
    });
});
