// button labels
export const SURVEY_START = 'Start';
export const SURVEY_NEXT = 'Next';
export const SURVEY_FINISH = 'Finish';
export const ERROR_MESSAGE_DEFAULT = 'This field is required';

// images
export const surveyImgMedium = '/waitingemail.png';
export const surveyImgLarge = '/waitingemail@2x.png';

export const logoMedium = '/logo-matic.png';
export const logoLarge = '/logo-matic@2x.png';

// survey text
export const getSurveyStartText = () =>
    'Matic would like to give you the best product. This survey will take few seconds.';
export const getSurveyEndText = (data) =>
    `Thanks for your answers, ${data.name}. We will call you at this number ${data.phone} within 24 hours`;