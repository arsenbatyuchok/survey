export const mockedForms = [
    {
        title: "What is your name?",
        fields: [
            {
                name: "firstName",
                label: "First name",
                type: "text",
                validation: "^.{3,128}$"
            }
       ]
    },
    {
        title: "How can we reach out to you?",
        fields: [
            {
                name: "phone",
                label: "Phone number",
                type: "phone",
                validation: "^\\(\\d{3}\\)\\s\\d{3}-\\d{4}"
            }
        ]
    },
    {
        title: "Which state is your house located in?",
        fields: [
            {
                name: "state",
                label: "State",
                type: "state"
            }
        ]
    }
];