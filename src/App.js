import React, { Component } from 'react';
import './App.css';
import Progress from './components/Progress/Progress';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Survey from './survey/Survey';


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      progressStatus: 0
    }

    this.setProgressStatus = this.setProgressStatus.bind(this);
  }

  setProgressStatus(progressStatus) {
    this.setState({ progressStatus });
  }

  render() {
      return (
        <div className="outer-wrapper">
          <Progress status={this.state.progressStatus} />
          <Header />
          <div className="content">
            <Survey onSectionChange={this.setProgressStatus} />
          </div>
          <Footer />
        </div>
      );
  }
}

export default App;
