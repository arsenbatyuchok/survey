import React from 'react';
import { shallow, mount } from 'enzyme';
import fetchMock from 'fetch-mock';
import Survey from './Survey';
import Wizard from '../components/Wizard/Wizard';
import MockClient from '../clients/MockClient';
import { mockedForms } from '../testData/mockedForms';

function flushPromises() {
    return new Promise(resolve => setImmediate(resolve));
}

describe('Survey', () => {
    const defaultProps = {
        onSectionChange: () => {}
    };

    beforeEach(() => {
        fetchMock.get(MockClient.constructFormsUrl(), {
            status: 200,
            body: mockedForms
        });
    });

    afterEach(() => {
        jest.clearAllMocks();
        fetchMock.restore();
    });

    it('renders itself and sets data after mount', async () => {
        const renderedComponent = mount(<Survey {...defaultProps} />);
        const spy = jest.spyOn(Survey, 'getSections');

        await flushPromises();
        renderedComponent.update();
        expect(renderedComponent.find('.survey').length).toEqual(1);
        expect(renderedComponent.state().isDataFetched).toEqual(true);
        expect(renderedComponent.state().sections.length).toEqual(5);
        expect(spy).toBeCalled();
    });

    it('renders children', async () => {
        const renderedComponent = shallow(<Survey {...defaultProps} />);

        await flushPromises();
        renderedComponent.update();
        expect(renderedComponent.find(Wizard).length).toEqual(1);
    });

    it('sets button text', async () => {
        expect(Survey.getButtonText(0, 5)).toEqual('Start');
        expect(Survey.getButtonText(3, 5)).toEqual('Finish');
    });

    // TODO test forms behavior and setting to formData
});