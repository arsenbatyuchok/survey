import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MockClient from './../clients/MockClient';
import Wizard from './../components/Wizard/Wizard';
import WizardForm from './../components/WizardForm/WizardForm';
import WizardText from './../components/WizardText/WizardText';
import { SURVEY_START, SURVEY_FINISH, surveyImgMedium, surveyImgLarge,
  getSurveyStartText, getSurveyEndText } from '../constants/constants';

class Survey extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDataFetched: false,
      sections: [],
      formData: {}
    }

    this.renderWizardSection = this.renderWizardSection.bind(this);
  }

  static image = {
    src: surveyImgLarge,
    srcSet: `${surveyImgMedium} 1x, ${surveyImgLarge} 2x`
  }

  static renderFormSection(section, onNext, buttonText) {
    return <WizardForm {...section} onSubmit={onNext} buttonText={buttonText} />
  }

  static renderTextSection(section, onNext, buttonText) {
    return <WizardText text={section.title} onNext={onNext} buttonText={buttonText} />
  }

  static getSections(forms) {
    const firstSection = { title: getSurveyStartText };
    const lastSection = { title: getSurveyEndText };

    return [firstSection, ...forms, lastSection];
  }

  static getButtonText(index, sectionsLength) {
    // TODO maybe refactor
    if (index === 0) {
      return SURVEY_START;
    } else if (index === sectionsLength - 2) {
      return SURVEY_FINISH;
    }
  }

  async componentDidMount() {
    const forms = await MockClient.getForms();

    this.setState({
      isDataFetched: true,
      sections: Survey.getSections(forms)
    });
  }

  onComplete() {
    // Submit code goes here...
  }

  collectFormData(goToNextSection) {
    const formData = {...this.state.formData};

    return (fields) => {
      const newData = Object.keys(fields).reduce((acc, item) => {
        acc[item] = fields[item];
        return acc;
      }, formData);
      this.setState({ formData: newData });
      goToNextSection();
    }
  }

  renderLastSection(section) {
    const { firstName, phone } = this.state.formData;

    return <WizardText text={section.title} textData={{ name: firstName, phone }}  />
  }

  renderWizardSection(idx, section, onNext) {
    const { sections } = this.state;
    const buttonText = Survey.getButtonText(idx, sections.length);

    if (section.fields && section.fields.length) {
      return Survey.renderFormSection(section, this.collectFormData(onNext), buttonText);
    } else if (idx === sections.length - 1) {
      return this.renderLastSection(section);
    }
    return Survey.renderTextSection(section, onNext, buttonText);
  }

  renderWizard() {
    const { sections } = this.state;
    const { onSectionChange } = this.props;

    return (
      <Wizard image={Survey.image} onComplete={this.onComplete} onSectionChange={onSectionChange} sections={sections}>
        {this.renderWizardSection}
      </Wizard>
    )
  }

  render() {
    return (
      <div className="survey panel">
        { this.state.isDataFetched && this.renderWizard() }
      </div>
    );
  }
}

Survey.propTypes = {
  onSectionChange: PropTypes.func
};

Survey.defaultProps = {
  onSectionChange: () => {}
};

export default Survey;
